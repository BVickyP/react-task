import { createStore, applyMiddleware, StoreEnhancer } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import rootReducer from "./rootReducer";
import rootSaga from './sagas';

export default function configureStore() {
  const composeEnhancers = composeWithDevTools({serialize: true});
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [sagaMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer];
  const enhancer: StoreEnhancer = composeEnhancers(...enhancers);

  const store = createStore(rootReducer, enhancer);

  sagaMiddleware.run(rootSaga);

  return store
}