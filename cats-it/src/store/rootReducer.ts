import { combineReducers } from "redux";

import auth from "./auth/reducer";
import cats from "./cats/reducer";

const rootReducer = combineReducers({
  auth,
  cats
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>