import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([
    helloSaga(),
    helloSaga2()
  ])
}

function* helloSaga() {
  console.log('Hello Sagas!')
}

function* helloSaga2() {
  console.log('Hello Sagas2!')
}